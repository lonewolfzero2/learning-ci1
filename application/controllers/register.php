<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	private $data = array();

	public function __construct()
	{
		parent::__construct();

		$this->data['judulhalaman'] = "Register";

		$this->load->library('form_validation');
		$this->load->model('model_crud');
	}	

	public function index()
	{
		$this->load->view('register', $this->data);
	}

	public function newuser()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');

		// if (isset($_POST['username'])) {
		// 	$username = $_POST['username'];
		// }
		if ( ! $this->form_validation->run()) {
			echo "Username telah terdaftar!";
		}
		else {
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$data['tanggal_daftar'] = date('Y-m-d');

			$result = $this->model_crud->insert('users', $data);

			if ($result) echo "Pendaftaran Berhasil!";
			else echo "Pendaftaran Gagal!";
		}
	}
}