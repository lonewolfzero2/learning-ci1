<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	private $data = array();

	public function __construct()
	{
		parent::__construct();

		$this->data['judulhalaman'] = "Login";

		$this->load->model('model_login');
	}	

	public function index()
	{
		$this->load->view('login', $this->data);
	}

	public function auth()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$result = $this->model_login->cek_login_user($username, $password);
		
		if ($result) echo "Login berhasil!";
		else echo "Login gagal!";
	}
}